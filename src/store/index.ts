import { createStore } from 'vuex'

const store = createStore({
  state() {
    return { num: 1 }
  },
})

export default store
