import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
  ErrorHandler,
} from 'vue-router'
// 引入路由守卫方法
import beforeEach from './beforeEach'
import afterEach from './afterEach'
const home = () => import('../components/Home.vue')
const login = () => import('../components/Login.vue')
const message = () => import('../components/Message.vue')
const page404 = () => import('../components/404.vue')

const routes = [
  {
    path: '/',
    redirect: '/message',
  },
  {
    path: '/message',
    name: 'message',
    component: message,
  },
  {
    path: '/login',
    name: 'login',
    component: login,
  },
  {
    path: '/home',
    name: 'home',
    component: home,
  },
  {
    path: '/404',
    name: '404',
    component: page404,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes: routes,
})

/**
 * 路由前置守卫
 */
router.beforeEach((guard) => {
  beforeEach.checkAuth(guard, router)
})

/**
 * 路由后置守卫
 */
router.afterEach((guard) => {
  afterEach.buriedPoint(guard, router)
})

/**
 * 路由错误回调
 */
router.onError((handler: ErrorHandler) => {
  console.log('error:', handler)
})

/**
 * 输出对象
 */
export default router
