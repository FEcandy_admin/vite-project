import { createApp } from 'vue'
import App from './App.vue'
import router from './route'
import store from './store' // store
import './common/reset.css'
import './common/common.css'
import 'animate.css/animate.min.css' //animate
import installElementPlus from './plugins/element' //element ui

import './assets/iconfont/iconfont.css'

const app = createApp(App)
installElementPlus(app)
app.use(store).use(router).mount('#app')
